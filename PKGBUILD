# Maintainer: ahrs <Forward dot to at hotmail dot co dot uk>

pkgname=undistract-me
pkgver=r79
pkgrel=1.3
arch=('any')
url="https://github.com/jml/undistract-me"
makedepends=('git' 'imagemagick')
license=("MIT")
pkgdesc="Notifies you when long-running terminal commands complete"
install=${pkgname}.install
source=("git+https://github.com/jml/undistract-me.git"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}.png"
        'ignore-windows-check.patch')
sha256sums=('SKIP'
            'c67bdc8d8f22254cf2806383135b58fdfe2795b22bca4647e4c208d327f81bbb'
            'e7b891cb89d20d64b537864dbcdc278096b44c51fb548252da81cdb4eea5be01'
            'e0710489eaf48490635097edf8bd8734dde43db7cee2cae4671cd4a45c50477f')

pkgver() {
    cd "$srcdir/undistract-me"
    printf "r%s" "$(git rev-list --count HEAD)"
}

prepare() {
    patch -p1 -i ./ignore-windows-check.patch
}

_undistract_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_undistract_desktop" | tee com.${pkgname/-/_}.desktop
}

package() {
    depends=('bash' 'libnotify' 'xorg-xprop')
    conflicts=('undistract-me-git')
    
    mkdir -p "$pkgdir/usr/share/undistract-me"
    mkdir -p "$pkgdir/usr/share/doc/undistract-me"
    mkdir -p "$pkgdir/etc/profile.d/"
    sed -ni '/Copyright/,/developers\"./p' "$srcdir/undistract-me/LICENSE"
    install -D -m644 "$srcdir/undistract-me/long-running.bash" "$pkgdir/usr/share/undistract-me/long-running.bash"
    install -D -m644 "$srcdir/undistract-me/preexec.bash" "$pkgdir/usr/share/undistract-me/preexec.bash"
    install -D -m644 "$srcdir/undistract-me/undistract-me.sh" "$pkgdir/etc/profile.d/undistract-me.sh"
    install -D -m644 "$srcdir/undistract-me/README.md" "$pkgdir/usr/share/doc/undistract-me/README.md"
    install -D -m644 "$srcdir/undistract-me/LICENSE" "$pkgdir/usr/share/doc/undistract-me/COPYING"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"

    for size in 16 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}"/usr/share/icons/hicolor/${size}x${size}/{apps,mimetypes}
        convert "${srcdir}/${pkgname}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}
